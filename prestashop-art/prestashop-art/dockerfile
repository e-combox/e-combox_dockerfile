FROM prestashop/prestashop:1.7.8.8

# Ajout des applications utiles
RUN apt update && apt dist-upgrade -y && apt install -y nano vim mariadb-client ssl-cert \
   && rm /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime \
   && dpkg-reconfigure --frontend noninteractive tzdata \
   && apt-get clean \
   && rm -r /var/lib/apt/lists/* \
   && a2enmod ssl \
   && a2ensite default-ssl

# Ajout de gosu
ARG GOSU_VERSION=1.17
RUN dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
   && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
   && chmod +x /usr/local/bin/gosu \
   && gosu nobody true

# Ajout des datas personnalisées
ADD prestashop-art-data.tar.gz /var/www/html/

# Permettre le changement d'URL entre 2 lancements de conteneurs et... divers
RUN rm -rf /var/www/html/install \
 && cp /tmp/docker_updt_ps_domains.php /var/www/html/ \
 && sed -ie "s/DirectoryIndex\ index.php\ index.html/DirectoryIndex\ docker_updt_ps_domains.php\ index.php\ index.html/g" /etc/apache2/conf-available/docker-php.conf \
 && rm -f /var/www/html/prestashop-art-data.tar.gz
  
# Ajout du script de préparation des images en vue d'une éventuelle duplication
COPY prepare-image.sh /tmp/
RUN chmod +x /tmp/prepare-image.sh

# Ajout du script qui change notamment les URL
COPY config-site.sh /tmp/
RUN chmod +x /tmp/config-site.sh

RUN sed -i '3a\. /tmp/config-site.sh\n' /usr/local/bin/docker-php-entrypoint \
   && sed -i '$i\if [ "$(id -u)" = "0" ]; then\n' /usr/local/bin/docker-php-entrypoint \
   && sed -i '$i\exec /usr/local/bin/gosu www-data "${BASH_SOURCE[0]}" "$@"\n' /usr/local/bin/docker-php-entrypoint \
   && sed -i '$i\fi\n' /usr/local/bin/docker-php-entrypoint

