#!/bin/bash

# En cas de de duplication, recopie des données dans le volume

SOURCE=/tmp/sources
BASE_HTML=/var/www/html

if [ -d "$SOURCE" ]; then

   if [ ! -e "$BASE_HTML/.copied" ]; then
      echo "Copie des fichiers" >>/tmp/config-site_1.log
      rm -rf ${BASE_HTML:?}/* $BASE_HTML/.[!.]*
      cp -pRf $SOURCE/* $SOURCE/.[!.]* $BASE_HTML/
      rm -rf $SOURCE
      touch $BASE_HTML/.copied
   fi
fi

# $ECB_DOCKER_HOST : adresse IP ou nom de domaine
# $NGINX_PORT : port d'écoute du reverse proxy
# $DB_NAME : nom du serveur de base de données

if [ -z "$ECB_DOCKER_HOST" ] || [ -z "$NGINX_PORT" ]; then
   echo "ERREUR, ECB_DOCKER_HOST et/ou NGINX_PORT est vide."
   echo "ERREUR, ECB_DOCKER_HOST et/ou NGINX_PORT est vide." >>/tmp/config-site_0.log
   exit 88
fi

TEMOIN=0
fin=$(date +"%s")
fin=$((fin + 300))
while [ $TEMOIN -eq 0 ] && [ "$(date +"%s")" -lt $fin ]; do
   mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "SELECT 1"
   if [ $? -eq 1 ]; then
      sleep 5
   else
      TEMOIN=1
      echo "SUCCESS, MySQL est prêt pour la suite"
      echo "SUCCESS, MySQL est prêt pour la suite" >>/tmp/config-site.log

      # Sauvegarde de l'ancien fichier .htaccess
      cp $BASE_HTML/.htaccess /tmp/htaccess.sauv

      # Mise à jour du fichier de paramètres avec le bon mot de passe
      sed -i "s/.*database_password.*/    'database_password' => '$DB_PASS',/g" $BASE_HTML/app/config/parameters.php

      ANCIEN_DOMAINE=$(cat $BASE_HTML/.htaccess | grep ^#Domain | cut -d" " -f2)
      echo "L'ancien domaine est $ANCIEN_DOMAINE" >/tmp/config-site.log

      # Vérification de l'activation de SSL : s'il n'est pas activé, on le fait
      TEMOIN_SSL=$(mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "SELECT VALUE FROM ps_configuration WHERE NAME = 'PS_SSL_ENABLED_EVERYWHERE';" -N -s)

      if [ "$TEMOIN_SSL" -eq 1 ]; then
         ANCIEN_PROTO=https
         echo "SSL est déjà activé"
         echo "SSL est déjà activé" >>/tmp/config-site.log
      else
         ANCIEN_PROTO=http
         # Activation dans la base de données
         mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "UPDATE ps_configuration SET VALUE = '1' WHERE NAME IN ('PS_SSL_ENABLED', 'PS_SSL_ENABLED_EVERYWHERE');"

         # Activation de HTTPS au niveau du fichier .htaccess
         sed -i "/RewriteEngine/a \SetEnvIf X-Forwarded-Proto https HTTPS=on" $BASE_HTML/.htaccess
         sed -i "/RewriteEngine/a \RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301,NE]" $BASE_HTML/.htaccess
         sed -i "/RewriteEngine/a \RewriteCond %{HTTP:X-Forwarded-Proto} \!https" .htaccess $BASE_HTML/.htaccess
         echo "SSL vient d'ềtre activé"
         echo "SSL vient d'ềtre activé" >>/tmp/config-site.log
      fi

      # Détermination de l'ancienne base
      ANCIENNE_BASE=$(mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "SELECT physical_uri FROM ps_shop_url;" -N -s)
      # Suppression des "/"
      if (echo "$ANCIENNE_BASE" | cut -d"/" -f3); then
         ANCIEN_CHEMIN=$(echo "$ANCIENNE_BASE" | cut -d"/" -f2)
         PARTIE2=$(echo "$ANCIENNE_BASE" | cut -d"/" -f3)
         ANCIENNE_BASE=$ANCIEN_CHEMIN/$PARTIE2
      else
         if (echo "$ANCIENNE_BASE" | cut -d"/" -f2); then
            ANCIENNE_BASE=$(echo "$ANCIENNE_BASE" | cut -d"/" -f2)
         fi
      fi
       if [ "$ANCIENNE_BASE" != "/" ]; then
         # On élimine éventuellement un / final
         ANCIENNE_BASE="${ANCIENNE_BASE%/}"
      fi

      echo "L'ancienne BASE est $ANCIENNE_BASE" >>/tmp/config-site.log

      # Détermination de l'ancienne URL
      if [ "$ANCIENNE_BASE" != "/" ]; then
         ANCIENNE_URL="$ANCIEN_PROTO://$ANCIEN_DOMAINE/$ANCIENNE_BASE/" >>/tmp/config-site.log
      else
         ANCIENNE_URL="$ANCIEN_PROTO://$ANCIEN_DOMAINE/" >>/tmp/config-site.log
      fi

      echo "L'ancienne URL est $ANCIENNE_URL" >>/tmp/config-site.log

      # Détermination de la nouvelle URL et du nouveau domaine
      if [ -z "$CHEMIN" ]; then
         NOUVELLE_URL="https://$ECB_DOCKER_HOST:$NGINX_PORT/$VIRTUAL_HOST/"
         NOUVEAU_DOMAINE="$ECB_DOCKER_HOST:$NGINX_PORT"
         NOUVELLE_BASE="$VIRTUAL_HOST"
      else
         NOUVELLE_URL="https://$DOMAINE/$CHEMIN/$VIRTUAL_HOST/"
         NOUVEAU_DOMAINE=$DOMAINE
         NOUVELLE_BASE="$CHEMIN/$VIRTUAL_HOST"
      fi

      {
         echo "La nouvelle URL doit être $NOUVELLE_URL"
         echo "Le nouveau domaine doit être $NOUVEAU_DOMAINE"
         echo "La nouvelle base doit être $NOUVELLE_BASE"
      } >>/tmp/config-site.log

      echo "Comparaison entre $ANCIENNE_URL et $NOUVELLE_URL" >>/tmp/config-site.lo

      if [ "$ANCIENNE_URL" != "$NOUVELLE_URL" ]; then

         # Un peu de ménage
         mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "TRUNCATE ps_linksmenutop;"
         mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "TRUNCATE ps_connections;"
         mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "TRUNCATE ps_connections_source;"
         mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "TRUNCATE ps_linksmenutop_lang;"
         mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "TRUNCATE ps_log;"

         # Suppression du cache
         rm -rf $BASE_HTML/var/cache/prod

         # Modification du fichier .htaccess
         sed -i "s|$ANCIEN_DOMAINE|$NOUVEAU_DOMAINE|g" $BASE_HTML/.htaccess
         echo "Premier IF, le fichier .htaccess a été modifié " >>/tmp/config-site.log

         # Mise à jour du domaine et du domaine_ssl dans MySQL
         mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "UPDATE ps_shop_url set domain='$NOUVEAU_DOMAINE';"
         mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "UPDATE ps_shop_url set domain_ssl='$NOUVEAU_DOMAINE';"

         # Mise à jour de MySQL si l'ancienne base est différente de la nouvelle
         if [ "$ANCIENNE_BASE" != "$NOUVELLE_BASE" ]; then
            mariadb -h "$DB_NAME" -u userPS -p"$DB_PASS" prestashop -e "UPDATE ps_shop_url set physical_uri='/$NOUVELLE_BASE/';"
            echo "Deuxième IF, pas de modification du fichier .htaccess" >>/tmp/config-site.log

            if [ "$ANCIENNE_BASE" != "/" ]; then
               sed -i "s|$ANCIENNE_BASE|$NOUVELLE_BASE|g" $BASE_HTML/.htaccess
               echo "Troisième IF, le fichier .htaccess a été modifié." >>/tmp/config-site.log
            else
               echo "Le ELSE" >>/tmp/config-sites.log
               sed -i "s|E=REWRITEBASE:/|E=REWRITEBASE:/$NOUVELLE_BASE/|g" $BASE_HTML/.htaccess
               sed -i "s|ErrorDocument 404 /index.php?controller=404|ErrorDocument 404 /$NOUVELLE_BASE/index.php?controller=404|g" $BASE_HTML/.htaccess
               echo "Le ELSE, le fichier .htaccess a été modifié " >>/tmp/config-site.log
            fi
         fi

         echo "La nouvelle URL est $NOUVELLE_URL"
         echo "La nouvelle URL est $NOUVELLE_URL" >>/tmp/config-site.log
         echo "Le fichier .htaccess a été regénéré."
         echo "Le fichier .htaccess a été regénéré." >>/tmp/config-site.log

      else
         echo "L'URL $ANCIENNE_URL n'a pas changé."
         echo "L'URL $ANCIENNE_URL n'a pas changé." >>/tmp/config-site.log
      fi
   fi
done

# Suppression du ou des anciens liens
if [ "$ANCIENNE_BASE" != "/" ]; then
   if [ -z "$ANCIEN_CHEMIN" ]; then
      if (ls $BASE_HTML/"$ANCIENNE_BASE" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE" >>/tmp/config-site.log
   else
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer"
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer" >>/tmp/config-site.log
   fi
   else
      if (ls $BASE_HTML/"$ANCIENNE_BASE" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE" >>/tmp/config-site.log
   else
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer"
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer" >>/tmp/config-site.log
   fi
      if (ls $BASE_HTML/"$ANCIEN_CHEMIN" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIEN_CHEMIN"
   fi
   fi
fi

# Création du ou des nouveaux liens
if [ -z "$CHEMIN" ]; then
   ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
else
   ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"
   ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
fi

if [ $TEMOIN -eq 0 ]; then
   echo "ERREUR, MySQL non encore actif."
   echo "ERREUR, MySQL non encore actif." >>/tmp/config-site.log
   exit 66
fi

# Lancement d'Apache avec l'utilisateur root
exec apache2-foreground