# syntax=docker/dockerfile:1

FROM alpine:3.6

# Définition de la branche
ENV BRANCHE 4.3

# Installation des paquets
RUN apk update && apk upgrade && apk add --no-cache git fcgiwrap spawn-fcgi nginx vim openssl

RUN mkdir /etc/ssl/ecombox \ 
 && openssl req -new -subj "/C=FR/ST=France/L=Corse/O=Reseaucerta/CN=localhost" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /etc/ssl/ecombox/ecombox.crt -keyout /etc/ssl/ecombox/ecombox.key \
 && chmod 400 /etc/ssl/ecombox/ecombox.key  

# Configuration de nginx pour git
COPY nginx.conf /etc/nginx/nginx.conf

# Le dossier où est créé le pid de nginx n'est pas créé par défaut sur cette distribution et initialisation du dossier des dépôts.
RUN mkdir -p /run/nginx && mkdir /git
WORKDIR /git
RUN git -c http.sslVerify=false clone --mirror https://forge.apps.education.fr/e-combox/e-comBox_docker-compose.git -b ${BRANCHE}
 
 # Droits nécessaire pour le dossier des dépôts (l'utilisateur nginx fait également parti du groupwww-data)
RUN chown -R nginx.nginx /git

# Gestion des logs de nginx
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
 && ln -sf /dev/stderr /var/log/nginx/error.log
 
# Les docker-compose se mettent à jour toutes les 15 minutes
RUN crontab -l | { cat; echo "*/5 * * * * cd /git/e-comBox_docker-compose.git/ && git -c http.sslVerify=false remote update"; } | crontab -

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

VOLUME /git 
EXPOSE 443

ENTRYPOINT ["/entrypoint.sh"] 
