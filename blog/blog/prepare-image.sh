#!/bin/bash

# Copie des données du volume pour qu'elles soient intégrées à l'image

SOURCE=/tmp/sources
BASE_HTML=/var/www/html

if [ -d "$SOURCE" ]; then
   rm -rf $SOURCE
fi

mkdir $SOURCE
cp -pRf $BASE_HTML/* $BASE_HTML/.[!.]* $SOURCE

#Suppression du ou des liens
if [ -z "$CHEMIN" ]; then
   rm $SOURCE/"$VIRTUAL_HOST"
else
   rm $SOURCE/"$VIRTUAL_HOST"
   rm $SOURCE/"$CHEMIN"

fi

if [ -e "$SOURCE/.copied" ]; then
   rm $SOURCE/.copied
fi
