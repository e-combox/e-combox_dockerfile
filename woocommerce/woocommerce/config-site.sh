#!/bin/bash

# En cas de de duplication, recopie des données dans le volume

SOURCE=/tmp/sources
BASE_HTML=/var/www/html

if [ -d "$SOURCE" ]; then

   if [ ! -e "$BASE_HTML/.copied" ]; then
      echo "Copie des fichiers" >>/tmp/config-site_0.log
      rm -rf ${BASE_HTML:?}/* $BASE_HTML/.[!.]*
      cp -pRf $SOURCE/* $SOURCE/.[!.]* $BASE_HTML/
      rm -rf $SOURCE
      touch $BASE_HTML/.copied
   fi
fi

# $ECB_DOCKER_HOST : adresse IP ou nom de domaine
# $NGINX_PORT : port d'écoute du reverse proxy
# $DB_NAME : nom du serveur de base de données

if [ -z "$ECB_DOCKER_HOST" ] || [ -z "$NGINX_PORT" ]; then
   echo "ERREUR, ECB_DOCKER_HOST et/ou NGINX_PORT est vide."
   echo "ERREUR, ECB_DOCKER_HOST et/ou NGINX_PORT est vide." >>/tmp/config-site_0.log
   exit 88
fi

TEMOIN=0
fin=$(date +"%s")
fin=$((fin + 300))
while [ $TEMOIN -eq 0 ] && [ "$(date +"%s")" -lt $fin ]; do
   mariadb -h "$DB_NAME" -u userWP -p"$DB_PASS" wordpress -e "SELECT 1"
   if [ $? -eq 1 ]; then
      sleep 5
   else
      TEMOIN=1
      echo "SUCCESS, MySQL est prêt pour la suite"
   fi
done

if [ $TEMOIN -eq 0 ]; then
   echo "ERREUR, MySQL non encore actif"
   exit 66
fi

# Mise à jour du wp-config
sed -i "s/.*DB_PASSWORD.*/define( 'DB_PASSWORD', '$DB_PASS');/g" /var/www/html/wp-config.php

ANCIENNE_URL=$(wp option get siteurl)

echo "L'ancienne URL est $ANCIENNE_URL" >/tmp/config-site.log

ANCIEN_CHEMIN=""
if [[ ! $(wp option get siteurl | cut -d"/" -f5 | grep -v '^[[:space:]]*$') = "" ]]; then
   ANCIEN_CHEMIN=$(wp option get siteurl | cut -d"/" -f4)
   PARTIE2=$(wp option get siteurl | cut -d"/" -f5)
   ANCIENNE_BASE=$ANCIEN_CHEMIN/$PARTIE2
else
   ANCIENNE_BASE=$(wp option get siteurl | cut -d"/" -f4)
fi

{
   echo "L'ancienne BASE est $ANCIENNE_BASE"
   if [ -z "$ANCIEN_CHEMIN" ]; then
      echo "Il n'y a pas d'ancien chemin."
   else
      echo "L'ancienne chemin est $ANCIEN_CHEMIN"
   fi
   echo "ECB_DOCKER_HOST est $ECB_DOCKER_HOST"
} >>/tmp/config-site.log

if [ -z "$CHEMIN" ]; then
   if [ "$NGINX_PORT" = "443" ]; then
      NOUVELLE_URL="https://$ECB_DOCKER_HOST/$VIRTUAL_HOST"
   else
      NOUVELLE_URL="https://$ECB_DOCKER_HOST:$NGINX_PORT/$VIRTUAL_HOST"
   fi
else
   NOUVELLE_URL="https://$DOMAINE/$CHEMIN/$VIRTUAL_HOST"
fi
echo "La nouvelle URL doit être $NOUVELLE_URL" >>/tmp/config-site.log

if [ ! "$NOUVELLE_URL" = "$ANCIENNE_URL" ]; then

   echo "On est dans le premier IF : la nouvelle URL doit être $NOUVELLE_URL/, il faut la changer"
   echo "On est dans le premier IF : la nouvelle URL doit être $NOUVELLE_URL/, il faut la changer" >>/tmp/config-site.log

   wp option update siteurl "$NOUVELLE_URL"/
   wp option update home "$NOUVELLE_URL"/
   wp search-replace "$ANCIENNE_URL" "$NOUVELLE_URL"

   NOUVELLE_URL=$(wp option get siteurl)

   echo "La nouvelle URL $NOUVELLE_URL/ a été mise à jour."
   echo "La nouvelle URL $NOUVELLE_URL/ a été mise à jour." >>/tmp/config-site.log

else

   echo "L'ancienne URL est $ANCIENNE_URL/"
   echo "La nouvelle URL est $NOUVELLE_URL/"
   echo "Il n'y a donc pas de changement d'URL"

   {
      echo "L'ancienne URL est $ANCIENNE_URL/"
      echo "La nouvelle URL est $NOUVELLE_URL/"
      echo "Il n'y a donc pas de changement d'URL"
   } >>/tmp/config-site.log
fi

# Suppression du ou des anciens liens
if [ -z "$ANCIEN_CHEMIN" ]; then
   if (ls $BASE_HTML/"$ANCIENNE_BASE" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE" >>/tmp/config-site.log
   else
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer"
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer" >>/tmp/config-site.log
   fi
else
   if (ls $BASE_HTML/"$ANCIENNE_BASE" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE" >>/tmp/config-site.log
   else
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer"
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer" >>/tmp/config-site.log
   fi
   if (ls $BASE_HTML/"$ANCIEN_CHEMIN" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIEN_CHEMIN"
   fi
fi

# Création du ou des nouveaux liens
if [ -z "$CHEMIN" ]; then
   ln -snfT $BASE_HTML/ $BASE_HTML/"$VIRTUAL_HOST"
else
   ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"
   ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"/"$VIRTUAL_HOST"
fi

echo "Création du ou des liens vers la nouvelle base $CHEMIN/$VIRTUAL_HOST"
echo "Création du ou des liens vers la nouvelle base $CHEMIN/$VIRTUAL_HOST" >>/tmp/config-site.log

# Lancement d'Apache avec l'utilisateur root
exec apache2-foreground
