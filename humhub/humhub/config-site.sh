#!/bin/sh

# En cas de de duplication, recopie des données dans le volume

SOURCE=/tmp/sources
BASE_HTML=/var/www/localhost/htdocs

if [ -d "$SOURCE" ]; then

   if [ ! -e "$BASE_HTML/.copied" ]; then
      echo "Copie des fichiers" >>/tmp/config-site_1.log
      rm -rf ${BASE_HTML:?}/* $BASE_HTML/.[!.]*
      cp -pRf $SOURCE/* $SOURCE/.[!.]* $BASE_HTML/
      rm -rf $SOURCE
      touch $BASE_HTML/.copied
   fi
fi

# Pas de récupération des anciennes valeurs, la ligne nginx est systématiquement réécrite avec la valeur de la nouvelle base

# Récupération des nouvelles valeurs (chemin éventuel, base et URL)
if [ -z "$CHEMIN" ]; then
   NOUVELLE_URL="https://$ECB_DOCKER_HOST:$NGINX_PORT/$VIRTUAL_HOST/"
   NOUVEAU_DOMAINE="$ECB_DOCKER_HOST:$NGINX_PORT"
   NOUVELLE_BASE="$VIRTUAL_HOST"
else
   NOUVELLE_URL="https://$DOMAINE/$CHEMIN/$VIRTUAL_HOST/"
   NOUVEAU_DOMAINE=$DOMAINE
   NOUVELLE_BASE="$CHEMIN/$VIRTUAL_HOST"
fi

echo "La nouvelle URL doit être $NOUVELLE_URL" >>/tmp/config-site.log

# Mise à jour de nginx.conf
sed -i "s|try_files \$uri \$uri/.*|try_files \$uri \$uri/ /$NOUVELLE_BASE/index.php;|g" /etc/nginx/nginx.conf

echo "Le fichier nginx.conf a été mis à jour avec la $NOUVELLE_BASE" >/tmp/config-site.log

# Mise à jour des fichiers de configuration
sed -i "s|'hostInfo'.*|'hostInfo' => 'https://$NOUVEAU_DOMAINE',|g" /var/www/localhost/htdocs/protected/config/common.php
sed -i "s|'baseUrl'.*|'baseUrl' => '/$NOUVELLE_BASE',|g" /var/www/localhost/htdocs/protected/config/common.php

sed -i "s|'hostInfo'.*|'hostInfo' => 'https://$NOUVEAU_DOMAINE',|g" /var/www/localhost/htdocs/protected/config/console.php
sed -i "s|'baseUrl'.*|'baseUrl' => '/$NOUVELLE_BASE',|g" /var/www/localhost/htdocs/protected/config/console.php

sed -i "s|'password'.*|'password' => '$DB_PASS',|g" /var/www/localhost/htdocs/protected/config/dynamic.php

echo "Les fichiers de configuration common.php et console.php ont été mis à jour avec la $NOUVELLE_BASE et le nouveau domaine $NOUVEAU_DOMAINE." >/tmp/config-site.log

# Suppression du ou des anciens liens
if [ -z "$ANCIEN_CHEMIN" ]; then
   if (ls $BASE_HTML/"$ANCIENNE_BASE" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE" >>/tmp/config-site.log
   else
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer"
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer" >>/tmp/config-site.log
   fi
else
   if (ls $BASE_HTML/"$ANCIENNE_BASE" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE" >>/tmp/config-site.log
   else
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer"
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer" >>/tmp/config-site.log
   fi
   if (ls $BASE_HTML/"$ANCIEN_CHEMIN" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIEN_CHEMIN"
   fi
fi

# Création du ou des nouveaux liens
if [ -z "$CHEMIN" ]; then
   ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
else
   ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"
   ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
fi

echo "Le ou les liens ont été créés" >>/tmp/config-site.log
