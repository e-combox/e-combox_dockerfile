#!/bin/bash

# Copie des données du volume pour qu'elles soient intégrées à l'image

SOURCE=/tmp/sources
BASE_HTML=/var/www/html/upload
BASE_CONFIG=/var/www/html/conf.d

if [ -d "$SOURCE" ]; then
   rm -rf $SOURCE
fi

mkdir -p $SOURCE/html $SOURCE/addons $SOURCE/config
cp -pRf $BASE_HTML/* $BASE_HTML/.[!.]* $SOURCE/html
cp -pRf $BASE_CONFIG/* $BASE_CONFIG/.[!.]* $SOURCE/config

if [ -e "$SOURCE/html/.copied" ]; then
   rm $SOURCE/html/.copied
fi
if [ -e "$SOURCE/config/.copied" ]; then
   rm $SOURCE/config/.copied
fi
