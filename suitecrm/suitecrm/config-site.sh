#!/bin/bash

SOURCE=/tmp/sources
BASE_HTML=/var/www/html/upload
BASE_CONFIG=/var/www/html/conf.d

if [ -d "$SOURCE" ]; then

  if [ ! -e "$BASE_HTML/.copied" ]; then
    echo "Copie des fichiers HTML" >>/tmp/config-site_1.log
    rm -rf ${BASE_HTML:?}/* $BASE_HTML/.[!.]*
    cp -pRf $SOURCE/html/* $SOURCE/html/.[!.]* $BASE_HTML/
    rm -rf $SOURCE/html
    touch $BASE_HTML/.copied
  fi

  if [ ! -e "$BASE_CONFIG/.copied" ]; then
    echo "Copie des fichiers HTML" >>/tmp/config-site_1.log
    rm -rf ${BASE_CONFIG:?}/* $BASE_CONFIG/.[!.]*
    cp -pRf $SOURCE/config/* $SOURCE/config/.[!.]* $BASE_CONFIG/
    rm -rf $SOURCE/config
    touch $BASE_CONFIG/.copied
    sed -i "s/.*db_password.*/    'db_password' => '$DB_PASS',/g" $BASE_CONFIG/config.php
  fi
  rm -rf $SOURCE
fi