#!/bin/bash

# En cas de de duplication, recopie des données dans le volume

SOURCE=/tmp/sources
BASE_HTML=/var/www/app
BASE_DATA=/var/www/app/data
BASE_PLUGINS=/var/www/app/plugins
BASE_CONF_NGINX=/etc/nginx

if [ -d "$SOURCE" ]; then

   if [ ! -e "$BASE_DATA/.copied" ]; then
      echo "Copie des fichiers data" >>/tmp/config-site_0.log
      rm -rf $BASE_DATA/data/* $BASE_DATA/data/.[!.]*
      cp -pRf $SOURCE/* $SOURCE/.[!.]* $BASE_DATA/
      rm -rf $SOURCE/data
      touch $BASE_DATA/.copied
   fi
   if [ ! -e "$BASE_PLUGINS/.copied" ]; then
      echo "Copie des fichiers plugins" >>/tmp/config-site_0.log
      rm -rf ${BASE_PLUGINS:?}/* $BASE_PLUGINS/.[!.]*
      cp -pRf $SOURCE/plugins/* $SOURCE/plugins/.[!.]* $BASE_PLUGINS/
      rm -rf $SOURCE/plugins
      touch $BASE_PLUGINS/.copied
   fi
   if [ ! -e "$BASE_CONF_NGINX/.copied" ]; then
      echo "Copie des fichiers conf de nginx" >>/tmp/config-site_0.log
      rm -rf ${BASE_CONF_NGINX:?}/* $BASE_CONF_NGINX/.[!.]*
      cp -pRf $SOURCE/conf/* $SOURCE/conf/.[!.]* $BASE_CONF_NGINX/
      rm -rf $SOURCE/conf
      touch $BASE_CONF_NGINX/.copied
   fi
fi

# $ECB_DOCKER_HOST : adresse IP ou nom de domaine
# $NGINX_PORT : port d'écoute du reverse proxy
# $DB_NAME : nom du serveur de base de données

if [ -z "$ECB_DOCKER_HOST" ] || [ -z "$NGINX_PORT" ]; then
   echo "ERREUR, ECB_DOCKER_HOST et/ou NGINX_PORT est vide."
   echo "ERREUR, ECB_DOCKER_HOST et/ou NGINX_PORT est vide." >>/tmp/config-site_0.log
   exit 88
fi

TEMOIN=0
fin=$(date +"%s")
fin=$((fin + 300))
while [ $TEMOIN -eq 0 ] && [ "$(date +"%s")" -lt $fin ]; do
   mariadb -h "$DB_NAME" -u userKB -p"$DB_PASS" kanboard -e "SELECT 1"
   if [ $? -eq 1 ]; then
      sleep 5
   else
      TEMOIN=1
      echo "SUCCESS, MySQL est prêt pour la suite"
   fi
done

if [ $TEMOIN -eq 0 ]; then
   echo "ERREUR, MySQL non encore actif"
   exit 66
fi

# Récupération des anciennes valeurs (chemin éventuel, base et URL)
ANCIENNE_URL=$(mariadb -h "$DB_NAME" -u userKB -p"$DB_PASS" kanboard -e "select value from settings where option='application_url';" -N -s)
echo "L'ancienne URL est $ANCIENNE_URL" >/tmp/config-site.log

TEST_ANCIENNE_URL=$(echo "$ANCIENNE_URL" | cut -d"/" -f5)

if [[ "$TEST_ANCIENNE_URL" =~ [^[:space:]] ]]; then
   ANCIEN_CHEMIN=$(mariadb -h "$DB_NAME" -u userKB -p"$DB_PASS" kanboard -e "select value from settings where option='application_url';" -N -s | cut -d"/" -f4)
   PARTIE2=$(mariadb -h "$DB_NAME" -u userKB -p"$DB_PASS" kanboard -e "select value from settings where option='application_url';" -N -s | cut -d"/" -f5)
   ANCIENNE_BASE=$ANCIEN_CHEMIN/$PARTIE2
   echo "On est dans le IF." >>/tmp/config-site.log
else
   ANCIENNE_BASE=$(mariadb -h "$DB_NAME" -u userKB -p"$DB_PASS" kanboard -e "select value from settings where option='application_url';" -N -s | cut -d"/" -f4)
   echo "On est dans le else." >>/tmp/config-site.log
fi

echo "L'ancienne BASE est $ANCIENNE_BASE" >>/tmp/config-site.log

echo "L'ancienne BASE est $ANCIENNE_BASE" >>/tmp/config-site.log

# Récupération des nouvelles valeurs (chemin éventuel, base et URL)
if [ -z "$CHEMIN" ]; then
   NOUVELLE_URL="https://$ECB_DOCKER_HOST:$NGINX_PORT/$VIRTUAL_HOST/"
   NOUVELLE_BASE="$VIRTUAL_HOST"
else
   NOUVELLE_URL="https://$DOMAINE/$CHEMIN/$VIRTUAL_HOST/"
   NOUVELLE_BASE="$CHEMIN/$VIRTUAL_HOST"
fi

echo "La nouvelle URL est $NOUVELLE_URL" >>/tmp/config-site.log
echo "La nouvelle BASE est $NOUVELLE_BASE" >>/tmp/config-site.log

# Mise à jour de Nginx
sed -i "s|.*try_files \$uri \$uri/.*|            try_files \$uri \$uri/ /$NOUVELLE_BASE/index.php;|" /etc/nginx/nginx.conf

# Mise à jour de nginx si les directives pour le HTTPS sont commentées (cas des sites provenant de la v3)
sed -i "s|#listen       443 ssl http2;|listen       443 ssl http2;|" /etc/nginx/nginx.conf
sed -i "s|#ssl_certificate /etc/nginx/ssl/kanboard.crt;|ssl_certificate /etc/nginx/ssl/kanboard.crt;|" /etc/nginx/nginx.conf
sed -i "s|#ssl_certificate_key /etc/nginx/ssl/kanboard.key;|ssl_certificate_key /etc/nginx/ssl/kanboard.key;|" /etc/nginx/nginx.conf

# Mise à jour de MySQL

echo "Comparaison entre $ANCIENNE_URL et $NOUVELLE_URL." >>/tmp/config-site.log

if [ ! "$ANCIENNE_URL" = "$NOUVELLE_URL" ]; then
   echo "On est dans le IF : la nouvelle URL est $NOUVELLE_URL, il faut la changer dans MySQL."
   echo "On est dans le IF : la nouvelle URL est $NOUVELLE_URL, il faut la changer dans MySQL." >>/tmp/config-site.log

   # Mise à jour de MySQL
   mariadb -h "$DB_NAME" -u userKB -p"$DB_PASS" kanboard -e "update settings s set s.value = '$NOUVELLE_URL' where s.option = 'application_url';"

else
   echo "L'ancienne URL est $ANCIENNE_URL."
   echo "La nouvelle URL est $NOUVELLE_URL."
   echo "Il n'y a donc pas de changement d'URL."
   echo "L'ancienne URL est $ANCIENNE_URL" >>/tmp/config-site.log

   echo "Il n'y a donc pas de changement d'URL." >>/tmp/config-site.log
fi

# Suppression du ou des anciens liens
if [ -z "$ANCIEN_CHEMIN" ]; then
   if (ls $BASE_HTML/"$ANCIENNE_BASE" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE" >>/tmp/config-site.log
   else
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer"
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer" >>/tmp/config-site.log
   fi
else
   if (ls $BASE_HTML/"$ANCIENNE_BASE" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE"
      echo "Suppression du lien vers l'ancienne base $ANCIENNE_BASE" >>/tmp/config-site.log
   else
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer"
      echo "Pas de lien vers l'ancienne base $ANCIENNE_BASE à supprimer" >>/tmp/config-site.log
   fi
   if (ls $BASE_HTML/"$ANCIEN_CHEMIN" >/dev/null 2>&1); then
      rm $BASE_HTML/"$ANCIEN_CHEMIN"
   fi
fi

# Création du ou des nouveaux liens
if [ -z "$CHEMIN" ]; then
   ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
else
   ln -snfT $BASE_HTML/ $BASE_HTML/"$CHEMIN"
   ln -snfT $BASE_HTML/ $BASE_HTML/"$NOUVELLE_BASE"
fi
echo "Le ou les anciens liens ont été créés." >>/tmp/config-site.log

# Reload de nginx
nginx -s reload
