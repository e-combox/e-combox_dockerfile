#!/bin/sh
# Pour que wp-cli s'exécute en tant que www-data
exec /usr/local/bin/gosu www-data "/bin/wp-cli.phar" "$@"
