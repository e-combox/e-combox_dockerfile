#!/bin/bash

# Copie des données du volume pour qu'elles soient intégrées à l'image

SOURCE=/tmp/sources
BASE_HTML=/var/lib/odoo
BASE_ADDONS=/mnt/extra-addons
BASE_CONFIG=/etc/odoo

if [ -d "$SOURCE" ]; then
   rm -rf $SOURCE
fi
   
mkdir -p $SOURCE/html $SOURCE/addons $SOURCE/config
cp -pRf $BASE_HTML/* $BASE_HTML/.[!.]* $SOURCE/html
cp -pRf $BASE_ADDONS/* $BASE_ADDONS/.[!.]* $SOURCE/addons
cp -pRf $BASE_CONFIG/* $BASE_CONFIG/.[!.]* $SOURCE/config
#rm $SOURCE/$VIRTUAL_HOST
if [ -e "$SOURCE/html/.copied" ]; then 
   rm $SOURCE/html/.copied
fi
if [ -e "$SOURCE/addons/.copied" ]; then 
   rm $SOURCE/addons/.copied
fi
if [ -e "$SOURCE/config/.copied" ]; then 
   rm $SOURCE/config/.copied
fi
